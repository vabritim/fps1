﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MapGeneration : MonoBehaviour
{

    public int width;
    public int height;

    public string seed;
    public bool useRandomSeed;

    [Range(0, 100)]
    public int randomFillPersent;

    int[,] map;

    // Use this for initialization
    void Start()
    {
        GenerateMap();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            GenerateMap();
        }
    }

    void GenerateMap()
    {
        map = new int[width, height];
        RandomFillMap();

        for(int i = 0; i < 5; i++)
        {
            SmoothMap();
        }

        MeshGenerate meshGen = GetComponent<MeshGenerate>();
        meshGen.GenerateMesh(map, 1);
    }
    
    void RandomFillMap()
    {
        if(useRandomSeed)
        {
            seed = Time.time.ToString();
        }

        System.Random pseudoRandom = new System.Random(seed.GetHashCode());

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                if((x == 0)||(x == width -1)||(y == 0)||(y == height - 1))
                {
                    map[x, y] = 1;
                }
                else
                {
                    map[x, y] = (pseudoRandom.Next(0, 100) < randomFillPersent) ? 1 : 0;
                }

            }
        }
    }
    void SmoothMap()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                int neightbourWallTile = GetSorroundWallCount(x, y);

                if(neightbourWallTile > 4)
                {
                    map[x, y] = 1;
                }
                else if(neightbourWallTile < 4)
                {
                    map[x, y] = 0;
                }
            }
        }
    }

    int GetSorroundWallCount(int gridX,int gridY)
    {
        int wallCount = 0;
        for(int neightbourX = gridX -1; neightbourX < gridX + 1; neightbourX++)
        {
            for (int neightbourY = gridY - 1; neightbourY < gridY + 1; neightbourY++)
            {
                if((neightbourX >= 0)&&(neightbourX < width)&& (neightbourY >= 0) && (neightbourY < height))
                {
                    if((neightbourX != gridX)&&(neightbourY != gridY))
                    {
                        wallCount += map[neightbourX, neightbourY];
                    }
                }
                else
                {
                    wallCount++;
                }

            }
        }

        return wallCount;
    }
    void OnDrawGizmos()
    {
        if(map != null)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    Gizmos.color = (map[x, y] == 1) ? Color.black : Color.white;
                    Vector3 pos = new Vector3(-width / 2 + x + 0.5f, 0, -height / 2 + y + 0.5f);
                    Gizmos.DrawCube(pos, Vector3.one);
                }
            }
        }
    }

}
